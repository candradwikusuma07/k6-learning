import http from "k6/http";
import { check } from "k6";

export default function () {
  const body = JSON.stringify({
    "username": "candra",
    "password": "12345"
  })

  const params = {
    headers: {
      "Content-Type": "application/json"
    }
  }
  const res = http.post('https://test-api.k6.io/auth/token/login/', body, params)
  check(res, {
    "response is 200": (r) => r.status === 200
  })
}