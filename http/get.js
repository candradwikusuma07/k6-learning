import http from 'k6/http'
import { check } from 'k6'

export default function () {
  let res = http.get('https://test-api.k6.io/public/crocodiles/')
  const crocodile = res.json()
  const crocodileId = crocodile[0].id
  const crocodileName = crocodile[0].name
  res = http.get(`https://test-api.k6.io/public/crocodiles/${crocodileId}/`)

  console.log(res.headers['Content-Type']); // access header

  check(res, {
    'status is 200': (r) => r.status === 200,
    // 'Crocodiles is Sobek': (r) => r.body.includes('Sobek'),
    'Crocodiles name': (r) => r.json().name === crocodileName
  })
}