import { check } from "k6";
import http from "k6/http";

export default function () {

  const credential = {
    username: 'test_' + Math.floor(Math.random() * 100000), //Date.now()
    password: 'secret_' + Date.now()
  }

  const body =
  {
    name: "candra_" + Date.now(),
    sex: "M",
    date_of_birth: "1995-12-31"
  }

  http.post('https://test-api.k6.io/user/register/',
    JSON.stringify(credential)
    , {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  let res = http.post('https://test-api.k6.io/auth/token/login/',
    JSON.stringify({
      "username": credential.username,
      "password": credential.password
    }), {
    headers: {
      "Content-Type": "application/json"
    }
  })
  const accessToken = res.json().access
  console.log(accessToken);

  http.get('https://test-api.k6.io/my/crocodiles/',
    {
      headers: {
        "Authorization": "Bearer " + accessToken
      }
    })

  res = http.post('https://test-api.k6.io/my/crocodiles/',
    JSON.stringify({
      name: "candra_" + Date.now(),
      sex: "M",
      date_of_birth: "1995-12-31"
    }),
    {
      headers: {
        "Authorization": "Bearer " + accessToken,
        "Content-Type": "application/json"
      }
    })
  const newCrocodileId = res.json().id

  res = http.get(`https://test-api.k6.io/my/crocodiles/${newCrocodileId}/`,
    {
      headers: {
        "Authorization": "Bearer " + accessToken
      }
    })

  check(res, {
    'should be 200': (r) => r.status === 200,
    'crocodile Id should be same': (r) => r.json().id === newCrocodileId
  })

  res = http.put(`https://test-api.k6.io/my/crocodiles/${newCrocodileId}/`,
    JSON.stringify({
      name: "candr_" + Date.now(),
      sex: "F",
      date_of_birth: "1994-12-31"
    }),
    {
      headers: {
        "Authorization": "Bearer " + accessToken,
        "Content-Type": "application/json"
      }
    })

  res = http.patch(`https://test-api.k6.io/my/crocodiles/${newCrocodileId}/`,
    JSON.stringify({

      sex: "M",

    }),
    {
      headers: {
        "Authorization": "Bearer " + accessToken,
        "Content-Type": "application/json"
      }
    })

  res = http.del(`https://test-api.k6.io/my/crocodiles/${newCrocodileId}/`,
    null,
    {
      headers: {
        "Authorization": "Bearer " + accessToken
      }
    })
}