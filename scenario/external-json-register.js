import http from 'k6/http';
import { randomString } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import { check } from 'k6';
import { SharedArray } from 'k6/data';


const userCredentials = new SharedArray('users with credential', () => {
  return JSON.parse(open('./json/users.json')).users
})

console.log(userCredentials);
export default function () {
  userCredentials.forEach((item) => {
    const credentials = {
      username: item.username,
      password: item.password,
    }

    let res = http.post(
      'https://test-api.k6.io/user/register/',
      JSON.stringify(credentials),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );

    check(res, {
      'status is 201': (r) => r.status === 201
    })
  })


  // res = http.post('https://test-api.k6.io/auth/token/login/',
  //   JSON.stringify({
  //     "username": "candra",
  //     "password": "12345"
  //   }),
  //   {
  //     headers: {
  //       "Content-Type": "application/json"
  //     }
  //   }
  // )
}