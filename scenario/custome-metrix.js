import { sleep } from "k6"
import http from "k6/http"
import { Counter, Trend } from "k6/metrics"

export const options = {
  vus: 10,
  duration: '10s',
  thresholds: {
    my_counter: ['count<=40'],
    my_trend: ['p(90)<600', 'p(95)<500'],
  }
}

let myCounter = new Counter('my_counter')
let myTrend = new Trend('my_trend')
export default function () {
  let res = http.get('https://test.k6.io/')
  myCounter.add(1)
  sleep(1)
  res = http.get('https://test.k6.io/news.php')
  myTrend.add(res.timings.duration)
  sleep(1)
}