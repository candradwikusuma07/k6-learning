import { sleep } from "k6";
import http from "k6/http";
import exec from 'k6/execution'

export const options = {
  vus: 10,
  duration: '30s'
}

export function setup() {
  let res = http.get('https://test.k6.local/some-page/status')
  if (res.error) {
    exec.test.abort('Aborting test application is Down')
  }
}

export default function () {
  http.get('https://test.k6.local/some-page')
  sleep(1)
}