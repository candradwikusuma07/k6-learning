import http from 'k6/http';
import { sleep, group, check } from 'k6';

export const options = {
  thresholds: {
    http_req_duration: ['p(95)<1000'],
    'http_req_duration{ expected_response:true }': ['p(95)<1000'],
    'group_duration{group:::Main page}': ['p(95)<8000'],
    'group_duration{group:::News page}': ['p(95)<7000'],
    'group_duration{group:::Main page::Assets}': ['p(95)<3000'],
  }
}

export default function () {

  group('Main page', function () {
    let res = http.get('https://run.mocky.io/v3/979d84e3-6d73-4ccd-adbe-8d4c6d5f5dd2?mocky-delay=900ms');
    check(res, { 'status is 200': (r) => r.status === 200 });

    group('Assets', function () {
      http.get('https://run.mocky.io/v3/979d84e3-6d73-4ccd-adbe-8d4c6d5f5dd2?mocky-delay=900ms');
      http.get('https://run.mocky.io/v3/979d84e3-6d73-4ccd-adbe-8d4c6d5f5dd2?mocky-delay=900ms');
    });
  });


  group('News page', function () {
    http.get('https://run.mocky.io/v3/b4abf102-76d6-4299-ac4c-c2365c2de278');
  });

  sleep(1);
}