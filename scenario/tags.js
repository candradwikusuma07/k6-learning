import http from 'k6/http'

export const options = {
  thresholds: {
    http_req_duration: ['p(95)<1000'],
    'http_req_duration{status:200}': ['p(95)<1000'],
    'http_req_duration{status:201}': ['p(95)<1000'],
  }
}
export default function () {
  http.get('https://run.mocky.io/v3/ebcf477d-837b-463a-bf99-e66a739635e3') // status 200
  http.get('https://run.mocky.io/v3/979d84e3-6d73-4ccd-adbe-8d4c6d5f5dd2?mocky-delay=2000ms') // status 201
}