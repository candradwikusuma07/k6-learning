import http from 'k6/http'
import { check } from 'k6' // for assertion
import { sleep } from 'k6'
import execution from 'k6/execution'
import { Counter } from 'k6/metrics' // custome counter matrics

import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

export const options = {
  // vus: 10,
  // duration: '10s',
  thresholds: { // for batasan yang kita inginkan
    http_req_duration: ['p(95)<500'], //trend
    http_req_duration: ['max<2000'], //trend
    http_req_failed: ['rate<0.01'], //rate
    http_reqs: ['count>20'], // counter
    http_reqs: ['rate>3'], //rate
    vus: ['value>9'], // gauges
    checks: ['rate>=0.98'],
    my_counter: ['count>=9'] //custome matrix
  },
  stages: [
    { duration: '60000', target: 10 }, // just slowly ramp-up to a HUGE load
  ],
}

let myCounter = new Counter('my_counter')

export default function () {
  const res = http.get('https://test.k6.io/')
  // const res = http.get('https://test.k6.io/' + (execution.scenario.iterationInTest === 1 ? 'foo' : ''))
  // console.log(execution.scenario.iterationInTest===1?'foo':'');
  myCounter.add(1) // custme matrics added
  check(res, {
    'status is 200': (r) => r.status === 200, // assertion
    'pages is startpage': (r) => r.body.includes('Collection of simple web-pages suitable for load testing.') // assertion
  })

  // sleep(2)


  //
}
export function handleSummary(data) {
  return {
    "script-result.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}