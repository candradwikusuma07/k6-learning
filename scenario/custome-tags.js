import http from 'k6/http';
import { Counter } from 'k6/metrics';
import { check, sleep } from 'k6';

export const options = {
  thresholds: {
    http_req_duration: ['p(95)<500'],
    'http_req_duration{page: order}': ['p(95)<500'],
    http_errors: ['count==0'],
    'http_errors{page: order}': ['count==0'],
    checks: ['rate>0.9'],
    'checks{page:order}': ['rate>0.9']
  }
}

let httpErrors = new Counter('http_errors');

export default function () {
  let res = http.get('https://run.mocky.io/v3/ebcf477d-837b-463a-bf99-e66a739635e3');

  if (res.error) {
    httpErrors.add(1);
  }

  check(res, {
    'status is 200': (r) => r.status === 200
  });

  // Submit order
  res = http.get(
    'https://run.mocky.io/v3/979d84e3-6d73-4ccd-adbe-8d4c6d5f5dd2?mocky-delay=2000ms',
    {
      tags: {
        page: 'order'
      }
    }
  );

  if (res.error) {
    httpErrors.add(1, { page: 'order' });
  }

  check(res, {
    'status is 201': (r) => r.status === 201
  }, { page: 'order' });

  sleep(1);
}