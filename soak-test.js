import http from 'k6/http'
import { sleep } from 'k6'

export const options = {
  stages: [
    {
      duration: '5m', // 5m
      target: 1000 // 100 -> slowly get data from 30, 50 100, 150 
    },
    {
      duration: '5h', // 30m
      target: 1000 // 100
    },
    {
      duration: '5m', //5m
      target: 0 // 0
    }
  ]

}

export default function () {
  http.get('https://test.k6.io')
  // http.get('https://test.k6.local') 
  sleep(1)
  http.get('https://test.k6.io/contacts.php')
  sleep(2)
  http.get('https://test.k6.io/news.php')
  sleep(2)
}