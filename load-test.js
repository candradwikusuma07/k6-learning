import http from 'k6/http'
import { sleep } from 'k6'

export const options = {
  stages: [
    {
      duration: '10s', // 5m
      target: 10 // 100 -> slowly get data from 30, 50 100, 150 
    },
    {
      duration: '30s', // 30m
      target: 10 // 100
    },
    {
      duration: '10s', //5m
      target: 0 // 0
    }
  ]

}

export default function () {
  http.get('https://test.k6.io')
  // http.get('https://test.k6.local') 
  sleep(1)
  http.get('https://test.k6.io/contacts.php')
  sleep(2)
  http.get('https://test.k6.io/news.php')
  sleep(2)
}



// import http from 'k6/http';
// import { sleep } from 'k6';

// export const options = {
//   // Key configurations for avg load test in this section
//   stages: [
//     { duration: '5m', target: 100 }, // traffic ramp-up from 1 to 100 users over 5 minutes.
//     { duration: '30m', target: 100 }, // stay at 100 users for 30 minutes
//     { duration: '5m', target: 0 }, // ramp-down to 0 users
//   ],
// };

// export default () => {
//   const urlRes = http.get('https://test-api.k6.io');
//   sleep(1);
//   // MORE STEPS
//   // Here you can have more steps or complex script
//   // Step1
//   // Step2
//   // etc.
// };